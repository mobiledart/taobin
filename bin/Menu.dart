import 'dart:io';


class Menuitem {
  var sum = [];
  var Item = [];
  var type = [];
  int price = 0 ;

  main(Item , sum){
    this.Item = Item;
    this.sum = sum;
  }

  void mainmenu(){
    print("Menu :\n1.เมนูแนะนำ\n2.กาแฟ \n3.ชา\n4.นมโกโก้ และ คาราเมล\n5.โปรตีนเชค\n6.น้ำโซดาและอื่นๆ\nกรุณาเลือกเมนู");
    print("---------------------------------------------------------------------");
  }

  String menuType(){
    print("---------------------------------------------------------------------");
    var Type1 = "ไม่ใส่น้ำตาล";
    var Type2 = "หวานน้อย";
    var Type3 = "หวานพอดี";
    var Type4 = "หวานมาก";
    var Type5 = "หวาน3โลก";
    print("กรุณาเลือกความหวาน\n1."+Type1+"\n2."+Type2+"\n3."+Type3+"\n4."+Type4+"\n5."+Type5+"\n6.ไม่เลือก");
    print("---------------------------------------------------------------------");
    String? x = stdin.readLineSync();
    print("---------------------------------------------------------------------");
    if(x == "1"){
      return Type1;
    }else if(x == "2"){
      return Type2;
    }else if(x == "3"){
      return Type3;
    }else if(x == "4"){
      return Type4;
    }else if(x == "5"){
      return Type5;
    }
    return "";
  }

  String menuTop(){
    var arr = [50,50,45,45,30];
    var menu1 = "ลาเต้เย็น";
    var menu2 = "โกโก้ปั่น";
    var menu3 = "ชนนมเย็น";
    var menu4 = "นมสด";
    var menu5 = "โค้ก";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  String menucoffee(){
    var arr = [50,45,50,45,55];
    var menu1 = "ลาเต้เย็น";
    var menu2 = "มอคค่าร้อน";
    var menu3 = "คาปูชิโน่เย็น";
    var menu4 = "เอสเพรสโซ่ร้อน";
    var menu5 = "ดับเบิ้ลเอสเพรสโซ่";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  String menutea(){
    var arr = [45,45,45,45,45];
    var menu1 = "เก็กฮวยร้อน";
    var menu2 = "น้ำขิงร้อน";
    var menu3 = "ชาไทยร้อน";
    var menu4 = "ชานมไต้หวันร้อน";
    var menu5 = "มัทฉะลาเต้ร้อน";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  String menusweet(){
    var arr = [45,45,45,50,40];
    var menu1 = "นมคาราเมลร้อน";
    var menu2 = "บราวน์ชูก้าร้อน";
    var menu3 = "โกโก้ร้อน";
    var menu4 = "คาราเมลโกโก้ร้อน";
    var menu5 = "นมร้อน";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  String menuprotein(){
    var arr = [55,55,65,50,45];
    var menu1 = "มัทฉะโปรตีน";
    var menu2 = "โกโก้โปรตีน";
    var menu3 = "สตอเบอร์รี่โปรตีน";
    var menu4 = "ชานมไทยโปรตีน";
    var menu5 = "โปรตีนเปล่า";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  String menusoda(){
    var arr = [40,10,45,40,30];
    var menu1 = "แดงโซดา";
    var menu2 = "โซดา";
    var menu3 = "กัญชาโซดา";
    var menu4 = "บ็วยโซดา";
    var menu5 = "โค้ก";
    addlist(arr, menu1, menu2, menu3, menu4, menu5);
    return "Error";
  }

  void printlist(){
    print("รายการสินค้า : ");
    price = 0;
    for(int i = 0 ; i < sum.length;i++){
      print(Item[i].toString()+" "+sum[i].toString()+" บาท " +type[i].toString());
      price += int.parse(sum[i].toString());
    }
    print("ราคารวม "+price.toString()+" บาท");
    print("---------------------------------------------------------------------");
  }

  void money(int x){
    int total = x-price;
    print(total.toString()+" บาท");
  }

  String casemainmenu(String menu){
    switch(menu){
      case "1":
        return menuTop();
      case "2":
        return menucoffee();
      case "3":
        return menutea();
      case "4":
        return menusweet();
      case "5":
        return menuprotein();
      case "6":
        return menusoda();
    }
    return "Error";
  }


  void addlist(arr,menu1,menu2,menu3,menu4,menu5){
    print("กรุณาเลือกสินค้า\n1."+menu1+" "+arr[0].toString()+"บาท \n2."+menu2+" "+arr[1].toString()+" บาท \n3."+menu3+" "+arr[2].toString()+" บาท\n4."+menu4+" "+arr[3].toString()+" บาท\n5."+menu5+" "+arr[4].toString()+" บาท");
    print("---------------------------------------------------------------------");
    String? x = stdin.readLineSync();
    switch(x){
      case "1":{
        sum.add(arr[0].toString());
        Item.add(menu1);
        type.add(menuType());
        printlist();
        return menu1+" "+arr[0].toString()+" บาท";
      }
      case "2":{
        sum.add(arr[1].toString());
        Item.add(menu2);
        type.add(menuType());
        printlist();
        return menu2+" "+arr[1].toString()+" บาท";
      }
      case "3":{
        sum.add(arr[2].toString());
        Item.add(menu3);
        type.add(menuType());
        printlist();
        return menu3+" "+arr[2].toString()+" บาท";
      }
      case "4":{
        sum.add(arr[3].toString());
        Item.add(menu4);
        type.add(menuType());
        printlist();
        return menu4+" "+arr[3].toString()+" บาท";
      }
      case "5":{
        sum.add(arr[4].toString());
        Item.add(menu5);
        type.add(menuType());
        printlist();
        return menu5+" "+arr[4].toString()+" บาท";
      }
    }
  }


}

